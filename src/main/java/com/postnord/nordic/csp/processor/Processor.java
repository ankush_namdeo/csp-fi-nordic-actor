package com.postnord.nordic.csp.processor;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.ValueMapper;

import com.postnord.potapi.util.SystemUtil;

import se.posten.loab.lisp.csp.potapi.domain.Event;


public class Processor {
	private static final String APPLICATION_ID = "asta-customs-output-actor";

	public void process() {
		KafkaStreams stream = topology();
		stream.start();
	}

	private KafkaStreams topology() {
		StreamsBuilder builder = new StreamsBuilder();
		builder.stream("csp-nordic-message", consumeResult()).filter(isNull()).mapValues(stringToJava()).filter(countryFilter()).to(outputTopic());;
		return null;
	}

	
	private String outputTopic() {
		return "prod-asta-se-customs-order-write";
	}

	private Predicate<? super String, ? super Event> countryFilter() {
		// TODO Auto-generated method stub
		return null;
	}

	private ValueMapper<String, Event> stringToJava() {
		// TODO Auto-generated method stub
		return new StringToJavaObject();
	}

	private Predicate<String, String> isNull() {
		return (k, v) -> v != null;
	}

	private Consumed<String, String> consumeResult() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();
		return Consumed.with(keySerde, valueSerde);
	}


}
